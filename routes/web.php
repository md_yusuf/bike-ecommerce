<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ReviewController;
use App\Http\Controllers\Site\CartController;
use App\Http\Controllers\Site\HomeController;
use App\Http\Middleware\onlyAdmin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/product/{id}', [HomeController::class, 'product']);
Route::post('/add_product/{id}', [CartController::class, 'add_product']);
Route::get('/cart/remove/{id}', [CartController::class, 'remove_product']);
Route::get('/cart/cart_remove_one_product/{id}',[CartController::class,'cart_remove_one_product']);
Route::get('/cart/cart_add_one_product/{id}',[CartController::class,'cart_add_one_product']);
Route::post('/cart/update_product/{id}', [CartController::class, 'update_product']);
Route::get('/cart', [CartController::class, 'cart']);
Route::post('/cart', [ReviewController::class, 'create']);
Route::get('/goCart', [CartController::class, 'goCart']);
Route::get('/checkout', [CartController::class, 'checkout']);
// Route::post('/product_review', [ReviewController::class, 'store']);






// Route::get('/', function () {
//     return view('site.home');
// });

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::prefix('/admin')->middleware(['auth', onlyAdmin::class])->group(function () {
    Route::get('/dashboard',[DashboardController::class, 'index']);
    Route::resource('/categories', CategoryController::class);
    Route::resource('/products', ProductController::class);

    Route::get('/reviews', [ReviewController::class, 'index'] );
    Route::get('/reviews/getData/{id}', [ReviewController::class, 'edit'] );
    Route::post('/reviews/update', [ReviewController::class, 'update'] );


});




require __DIR__.'/auth.php';

