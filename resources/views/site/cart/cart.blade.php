@extends('site.layouts.app')
@section('content')

 <!--Breadcrumb section-->
 <div class="breadcrumb_section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_inner">
                    <ul>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><i class="zmdi zmdi-chevron-right"></i></li>
                        <li>Cart</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumb section end-->

    <!-- PAGE SECTION START -->
    <div class="cart_page_area pt-100 pb-60">
       <form action="#">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="cart-table table-responsive mb-40">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="pro-thumbnail">Image</th>
                                        <th class="pro-title">Product Name</th>
                                        <th class="pro-price">Price</th>
                                        <th class="pro-quantity">Quantity</th>
                                        <th>Product Size</th>
						                <th>Product Colour</th>
                                        <th class="pro-subtotal">Total</th>
                                        <th class="pro-remove">Remove</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cartCollection as $cart)
                                        
                                    
                                    <tr>
                                        <td class="pro-thumbnail"><a href="#"><img src="{{ asset("/storage/".$cart->attributes->featured_image) }}" alt="" /></a></td>
                                        <td class="pro-title"><a href="#">{{$cart->name}}</a></td>
                                        <td class="pro-price"><span class="amount">${{$cart->price}}</span></td>

                                        <td class="product-quantity" data-title="Quantity">
                                            <div class="quantity-box type1">
                                                <div class="qty-input">
                                                    <input type="text" name="qty12554" value="{{ $cart->quantity }}"
                                                        data-max_value="20" data-min_value="1" data-step="1">
                                                    <a href="{{ url("/cart/cart_add_one_product/$cart->id") }}"
                                                        class="btn btn-up"><i class="fa fa-plus"></i></a>
                                                    <a href="{{ url("/cart/cart_remove_one_product/$cart->id") }}"
                                                        class="btn"><i class="fa fa-minus"></i></a>
                                                </div>
                                            </div>
                                        </td>
                                        
                                        {{-- <td class="quantity">
                                            <div class="quantity-select" >                           
                                                <a href="{{ url("/cart/cart_remove_one_product/$cart->id") }}" >
                                                    <div class="entry value-minus" style="background-color: rgb(16, 77, 56)" >&nbsp;</div>
                                                </a>
                                                <div class="entry value"><span>{{ $cart->quantity }}</span></div>
                                                <a href="{{ url("/cart/cart_add_one_product/$cart->id") }}">
                                                    <div class="entry value-plus active">&nbsp;</div>
                                                </a>                                               
                                            </div>
                                        </td> --}}
                                        <td class="pro-size">{{ $cart->attributes->size }}</td>
						                <td class="pro-color">{{ $cart->attributes->colour }}</td>
                                        <td class="pro-subtotal">{{ $cart->price *$cart->quantity }}</td>
                                        <td class="pro-remove"><a href="{{ url("/cart/remove/$cart->id") }}">×</a></td>
                                    </tr>   
                                    @endforeach                            
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-8 col-12">
                        <div class="cart-buttons mb-30">
                            <input type="submit" value="Update Cart" />
                            <a href="#">Continue Shopping</a>
                        </div>
                        <div class="cart-coupon mb-40">
                            <h4>Coupon</h4>
                            <p>Enter your coupon code if you have one.</p>
                            <div class="coupon_form_inner">
                                <input type="text" placeholder="Coupon code" />
                                <input type="submit" value="Apply Coupon" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="cart-total mb-40">
                            <h3>Cart Totals</h3>
                            {{-- <ul>
                                @php
                                    $total=0;
                                @endphp
                                @foreach ($cartCollection as $cart)  
                                <li>{{ $cart->name }} <i>-</i> <span>{{ $cart->price *$cart->quantity }}</span></li>
                                @php
                                    $total+=$cart->price *$cart->quantity;
                                @endphp
                                @endforeach
                                <hr>
                                <li>Total <i>-</i> <span>{{ $total }}</span></li>
                            </ul> --}}
                            <div class="table-responsive">
                                <table>
                                    <tbody>
                                        <tr class="cart-subtotal">
                                            <th>Subtotal</th>
                                            <td>
                                                @php
                                                $total=0;
                                                @endphp
                                                @foreach ($cartCollection as $cart)  
                                                    
                                                    <li>{{ $cart->name }} <i>-</i> <span>${{ $cart->price *$cart->quantity }}</span></li>

                                                @php
                                                    $total+=$cart->price *$cart->quantity;
                                                @endphp
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr class="order-total">
                                            <th>Total</th>
                                            <td>
                                                <strong><span class="amount">${{ $total }}</span></strong>
                                            </td>
                                        </tr>											
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="proceed-to-checkout section mt-30">
                                <a href="#">Proceed to Checkout</a>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </form>	
    </div>
    <!-- PAGE SECTION END --> 

@endsection