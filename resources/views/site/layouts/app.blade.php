<!doctype html>
<html class="no-js" lang="zxx">

<!-- Mirrored from preview.hasthemes.com/exporso-preview/exporso/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Mar 2022 14:46:16 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bike-ecommerce, Accessories Store eCommerce Bootstrap 4 Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset("site/img/favicon.png") }}">

    <!-- all css here -->
    <link rel="stylesheet" href="{{ asset("site/css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("site/css/plugin.css") }}">
    <link rel="stylesheet" href="{{ asset("site/css/bundle.css") }}">
    <link rel="stylesheet" href="{{ asset("site/css/style.css") }}">
    <link rel="stylesheet" href="{{ asset("site/css/responsive.css") }}">

    <script src="{{ asset("site/js/vendor/modernizr-2.8.3.min.js") }}"></script>
</head>

<body>
    <!-- Add your site or application content here -->

    <div class="exporso_wrapper">
        @include('site.layouts.topmenu')

        @yield('content')
        <!--Newsletter section start -->
        <div class="newsletter_section ptb-80">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-12">
                        <div class="newsletter_text">
                            <h2>Get All Updates</h2>
                            <p>Sign up our newsleter today. Also get allert for new product.</p>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12">
                        <div class="newsletter_form">
                            <form action="#">
                                <input type="email" placeholder="Type your email">
                                <button type="submit">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--Newsletter section end -->
        <!--Footer start-->
        <footer class="footer_area">
            <div class="footer_top">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="our_help_services ptb-80">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <div class="help_service d-flex">
                                            <div class="h_ser_icon">
                                                <i class="zmdi zmdi-boat"></i>
                                            </div>
                                            <div class="h_ser_text">
                                                <h3>Free Shipping</h3>
                                                <p>Free Shipping on Chicago</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <div class="help_service d-flex justify-content-center">
                                            <div class="h_ser_icon">
                                                <i class="zmdi zmdi-shield-security"></i>
                                            </div>
                                            <div class="h_ser_text">
                                                <h3>Money Guarentee</h3>
                                                <p>Free Shipping on Chicago</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <div class="help_service d-flex justify-content-end">
                                            <div class="h_ser_icon">
                                                <i class="zmdi zmdi-phone-setting"></i>
                                            </div>
                                            <div class="h_ser_text">
                                                <h3>Online Support</h3>
                                                <p>Free Shipping on Chicago</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bottom ptb-80">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="single_footer widget_description">
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                                    suffered.</p>
                                <div class="addresses_inner">
                                    <div class="single_address">
                                        <p>
                                            <span> Address: </span> <span>Your address goes here.</span>
                                        </p>

                                        <p>
                                            <span> Phone: </span> <span>+12 2223 2223 22 <br> +125 5455 5555 55</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="social__icon">
                                    <ul>
                                        <li>
                                            <a class="facebook" href="#" title="Facebook"><i
                                                    class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a class="google-plus" href="#" title="Google Plus"><i
                                                    class="fa fa-google-plus"></i></a>
                                        </li>

                                        <li>
                                            <a class="twitter" href="#" title="Twitter"><i
                                                    class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a class="linkedin" href="#" title="Linkedin"><i
                                                    class="fa fa-linkedin"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single_footer footer_widget_menu">
                                <div class="widget_title">
                                    <h3>Services</h3>
                                </div>
                                <ul>
                                    <li><a href="#">free shipping</a></li>
                                    <li><a href="#">Product Delivary</a></li>
                                    <li><a href="#">Product Tracking</a></li>
                                    <li><a href="#">Online Pyament</a></li>
                                    <li><a href="#">Discount</a></li>
                                    <li><a href="#">Online Vendor</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single_footer footer_widget_menu">
                                <div class="widget_title">
                                    <h3>Support</h3>
                                </div>
                                <ul>
                                    <li><a href="#">QUeality</a></li>
                                    <li><a href="#">Order Details</a></li>
                                    <li><a href="#">Order Slips</a></li>
                                    <li><a href="#">Shipping</a></li>
                                    <li><a href="#">Store Deal</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single_footer footer_widget_menu">
                                <div class="widget_title">
                                    <h3>Account</h3>
                                </div>
                                <ul>
                                    <li><a href="#">My account </a></li>
                                    <li><a href="#">order history</a></li>
                                    <li><a href="#">wishslist</a></li>
                                    <li><a href="#">Cart Details</a></li>
                                    <li><a href="#">Compare</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="copyright_inner text-center">
                                <p>© 2021 Exporso Mede with ❤️ by <a class="text-white" href="https://hasthemes.com/"
                                        target="_blank">HasThemes</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--Footer end-->

    </div>

    <!-- all js here -->
    <script src="{{ asset("site/js/vendor/jquery-1.12.0.min.js") }}"></script>
    <script src="{{ asset("site/js/popper.js") }}"></script>
    <script src="{{ asset("site/js/bootstrap.min.js") }}"></script>
    <script src="{{ asset("site/js/plugins.js") }}"></script>
    <script src="{{ asset("site/js/main.js") }}"></script>
</body>

<!-- Mirrored from preview.hasthemes.com/exporso-preview/exporso/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Mar 2022 14:46:29 GMT -->

</html>