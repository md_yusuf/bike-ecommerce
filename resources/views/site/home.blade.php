@extends('site.layouts.app')
@section('content')
  <!-- slider-area-start -->
  <div class="slider_wrapper">
    <div class="slider-active owl-carousel">
        <!--Single Slide-->
        <div class="single__slider bg-opacity" style="background-image:url(site/img/slide/1.jpg)">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <div class="slider-content slider-text-animation">
                            <h1>Best Deal</h1>
                            <h2>15% - 20% Off</h2>
                            <p>Simply dummy text of the printing and typesetting. </p>
                            <a href="#">Buy Now</a>									
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="slider_layer_image">
                            <img src="site/img/slide/layer_img_1.png" alt="">
                        </div>
                    </div>
                </div>	
            </div>
        </div>
        <!--Single slide end--> 
        <!--Single Slide-->
        <div class="single__slider bg-opacity" style="background-image:url(site/img/slide/1.jpg)">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <div class="slider-content slider-text-animation">
                            <h1>Best Deal</h1>
                            <h2>15% - 20% Off</h2>
                            <p>Simply dummy text of the printing and typesetting. </p>
                            <a href="#">Buy Now</a>									
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="slider_layer_image">
                            <img src="site/img/slide/layer_img_2.png" alt="">
                        </div>
                    </div>
                </div>	
            </div>
        </div>
        <!--Single slide end--> 
        <!--Single Slide-->
        <div class="single__slider bg-opacity" style="background-image:url(site/img/slide/1.jpg)">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <div class="slider-content slider-text-animation">
                            <h1>Best Deal</h1>
                            <h2>15% - 20% Off</h2>
                            <p>Simply dummy text of the printing and typesetting. </p>
                            <a href="#">Buy Now</a>									
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="slider_layer_image">
                            <img src="site/img/slide/layer_img_3.png" alt="">
                        </div>
                    </div>
                </div>	
            </div>
        </div>
        <!--Single slide end-->    
    </div>
</div>
<!-- slider-area-end -->

<!--Banner area start-->
<div class="banner_area pt-50">
    <div class="banner_area_inner d-flex">
        <div class="col_4">
            <div class="single_banner">
                <a href="#"><img src="site/img/banner/1.jpg" alt=""></a>
            </div>
        </div>
        <div class="col_4">
            <div class="single_banner">
                <a href="#"><img src="site/img/banner/2.jpg" alt=""></a>
            </div>
        </div>
        <div class="col_4">
            <div class="single_banner">
                <a href="#"><img src="site/img/banner/3.jpg" alt=""></a>
            </div>
        </div>
    </div>
</div>
<!--Banner area end-->


<!--Hot Deal product start-->
<div class="hot_details_product pt-110 pb-107">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-lg-3 col-md-3 col-12">
                <div class="section_title">
                    <h2>Hot Deals</h2>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="nav product_tab_menu justify-content-end" role="tablist">
                    <a class="active" href="#hot_all" data-toggle="tab" role="tab" aria-selected="true" aria-controls="hot_all">All</a>
                    <a href="#hot_bike" data-toggle="tab" role="tab" aria-selected="false" aria-controls="hot_bike">Bike</a>
                    <a href="#hot_tiar" data-toggle="tab" role="tab" aria-selected="false" aria-controls="hot_tiar">Tiar</a>
                    <a href="#hot_parts" data-toggle="tab" role="tab" aria-selected="false" aria-controls="hot_parts">Parts</a>
                    <a href="#hot_wheel" data-toggle="tab" role="tab" aria-selected="false" aria-controls="hot_wheel">cycle</a>
                    <a href="#hot_light" data-toggle="tab" role="tab" aria-selected="false" aria-controls="hot_light">Engine</a>
                </div>
            </div>
        </div>
        <div class="row mt-60">
            <div class="col-lg-9 col-md-12 ">
                <div class="tab-content">
                    <div class="tab-pane active show fade" id="hot_all" role="tabpanel">
                        <div class="row">
                       
                            @foreach ($hotDeals as $p)
                                <div class="col-lg-4 col-md-6">
                                    <div class="single__product">
                                        <div class="produc_thumb">
                                            <a href="{{ asset("/product/$p->id")}}"><img src="{{asset("storage/$p->featured_image") }}" alt=""></a>
                                        </div>
                                        <div class="product_hover">
                                            <div class="product_action">
                                                <form action="{{ url("/add_product/$p->id") }}" method="POST">
                                                    @csrf
                                                    <button type="submit" class="btn btn-outline-danger" > Add to Cart</button>
                                                </form>
                                                
                                                <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                            </div>
                                            <div class="product__desc">
                                                <h3><a href="product-details.html">{{ $p->name}}}</a></h3>
                                                <div class="price_amount">
                                                    <span class="current_price">${{$p->PriceAfterDiscount}}</span>
                                                    <span class="discount_price">${{ $p->discount_amount }}</span>
                                                    <span class="old_price">${{ $p->price }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            @endforeach
                                                    
                        </div>
                    </div>
                    <div class="tab-pane fade" id="hot_bike" role="tabpanel">

                        <div class="row">
                            @foreach($bike_product as $bike)
                            <div class="col-lg-4 col-md-6">
                           
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="{{ asset("/product/$bike->id")}}"><img src="{{asset("storage/$bike->featured_image") }}" alt=""></a>
                                        
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <form action="{{ url("/add_product/$bike->id") }}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-outline-danger" > Add to Cart</button>
                                            </form>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">{{$bike->name}}</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">${{$bike->PriceAfterDiscount}}</span>
                                                <span class="discount_price">${{$bike->discount_amount}}</span>
                                                <span class="old_price">${{$bike->price}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

              
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane fade" id="hot_tiar" role="tabpanel">
                        <div class="row">
                            @foreach($bike_product2 as $bike)
                            <div class="col-lg-4 col-md-6">
                           
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="{{ asset("/product/$bike->id")}}"><img src="{{asset("storage/$bike->featured_image") }}" alt=""></a>
                                        
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <form action="{{ url("/add_product/$bike->id") }}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-outline-danger" > Add to Cart</button>
                                            </form>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">{{$bike->name}}</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

              
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane fade" id="hot_parts" role="tabpanel">
                        <div class="row">
                            @foreach($bike_product3 as $bike)
                            <div class="col-lg-4 col-md-6">
                           
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="{{ asset("/product/$bike->id")}}"><img src="{{asset("storage/$bike->featured_image") }}" alt=""></a>
                                        
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <form action="{{ url("/add_product/$bike->id") }}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-outline-danger" > Add to Cart</button>
                                            </form>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">{{$bike->name}}</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">${{$bike->PriceAfterDiscount}}<</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

              
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane fade" id="hot_wheel" role="tabpanel">
                        <div class="row">
                            @foreach($bike_product4 as $bike)
                            <div class="col-lg-4 col-md-6">
                           
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="{{ asset("/product/$bike->id")}}"><img src="{{asset("storage/$bike->featured_image") }}" alt=""></a>
                                        
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <form action="{{ url("/add_product/$bike->id") }}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-outline-danger" > Add to Cart</button>
                                            </form>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">{{$bike->name}}</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">${{$bike->PriceAfterDiscount}}</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

              
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane fade" id="hot_light" role="tabpanel">
                        <div class="row">
                            @foreach($bike_product5 as $bike)
                            <div class="col-lg-4 col-md-6">
                           
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="{{ asset("/product/$bike->id")}}"><img src="{{asset("storage/$bike->featured_image") }}" alt=""></a>
                                        
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <form action="{{ url("/add_product/$bike->id") }}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-outline-danger" > Add to Cart</button>
                                            </form>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">{{$bike->name}}</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

              
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-3 col-md-12">
                <div class="single_banner long_hot_detals d-lg-none">
                    <a href="#"><img src="site/img/banner/banner_tab_1.jpg" alt="Shop Banner"></a>
                </div>
                <div class="single_banner long_hot_detals d-none d-lg-block">
                    <a href="#"><img src="site/img/banner/4.jpg" alt="Shop Banner"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Hot Deal product end-->  

                          
<!--Hot Deal product start-->
<div class="hot_details_product pb-110">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-lg-3 col-md-3 col-12">
                <div class="section_title">
                    <h2>Best Product</h2>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="nav product_tab_menu justify-content-end" role="tablist">
                    <a class="active" href="#best_all" data-toggle="tab" role="tab" aria-selected="true" aria-controls="best_all">All</a>
                    <a href="#best_bike" data-toggle="tab" role="tab" aria-selected="false" aria-controls="best_bike">Bike</a>
                    <a href="#best_tiar" data-toggle="tab" role="tab" aria-selected="false" aria-controls="best_tiar">Tiar</a>
                    <a href="#best_parts" data-toggle="tab" role="tab" aria-selected="false" aria-controls="best_parts">Parts</a>
                    <a href="#best_wheel" data-toggle="tab" role="tab" aria-selected="false" aria-controls="best_wheel">Wheel</a>
                    <a href="#best_light" data-toggle="tab" role="tab" aria-selected="false" aria-controls="best_light">Light</a>
                </div>
            </div>
        </div>
        <div class="row mt-60">
            <div class="col-lg-9 col-md-12 ">
                <div class="tab-content">
                    <div class="tab-pane active show fade" id="best_all" role="tabpanel">
                        <div class="row ">
                            @foreach ($bestProduct as $bp)
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="{{ asset("/product/$p->id")}}"><img src="{{asset("storage/$bp->featured_image")}}" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">{{$bp->name}}</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">${{$bp->PriceAfterDiscount}}</span>
                                                <span class="discount_price">${{$bp->discount_amount}}</span>
                                                <span class="old_price">${{$bp->price}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            @endforeach                         
                        </div>
                    </div>
                    <div class="tab-pane fade" id="best_bike" role="tabpanel">
                        <div class="row carousel_product owl-carousel">
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/1.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/2.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/4.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="best_tiar" role="tabpanel">
                        <div class="row carousel_product owl-carousel">
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/1.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/2.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/4.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="best_parts" role="tabpanel">
                        <div class="row ">
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/1.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/2.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/4.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="best_wheel" role="tabpanel">
                        <div class="row carousel_product owl-carousel">
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/1.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/2.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/4.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="best_light" role="tabpanel">
                        <div class="row carousel_product owl-carousel">
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/1.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/2.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single__product">
                                    <div class="produc_thumb">
                                        <a href="product-details.html"><img src="site/img/product/4.png" alt=""></a>
                                    </div>
                                    <div class="product_hover">
                                        <div class="product_action">
                                            <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                            <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                                        </div>
                                        <div class="product__desc">
                                            <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                            <div class="price_amount">
                                                <span class="current_price">$2999.99</span>
                                                <span class="discount_price">-08%</span>
                                                <span class="old_price">$3700.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-3 col-12 hot_righr_sidebar">
                <div class="single_banner long_hot_detals d-lg-none">
                    <a href="#"><img src="site/img/banner/banner_tab_2.png" alt="Shop Banner"></a>
                </div>
                <div class="single_banner  d-lg-block d-none">
                    <a href="#"><img src="site/img/banner/5.jpg" alt="Shop Banner"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Hot Deal product end-->


<!--Banner product section-->
<div class="banner_product_section pb-110">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="single_banner banner_length">
                            <a href="#"><img src="site/img/banner/6.jpg" alt="Shop Banner"></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="single_banner banner_length">
                            <a href="#"><img src="site/img/banner/7.jpg" alt="Shop Banner"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-12 col-12">
                <div class="best_seller_product_carousel owl-carousel">
                    <div class="best_selling_single">
                        <div class="single__product_sm mb-30">
                            <div class="produc_thumb">
                                <a href="product-details.html"><img src="site/img/product/pro_sm_1.png" alt=""></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3> 
                                <div class="product_ratting">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>

                        </div>
                        <div class="single__product_sm mb-30">
                            <div class="produc_thumb">
                                <a href="product-details.html"><img src="site/img/product/pro_sm_2.png" alt=""></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                <div class="product_ratting">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                </div>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>

                        </div>
                        <div class="single__product_sm">
                            <div class="produc_thumb">
                                <a href="product-details.html"><img src="site/img/product/pro_sm_3.png" alt=""></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="#">Lotafaj una khdii</a></h3>
                                <div class="product_ratting">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="best_selling_single">
                        <div class="single__product_sm mb-30">
                            <div class="produc_thumb">
                                <a href="product-details.html"><img src="site/img/product/pro_sm_1.png" alt=""></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3> 
                                <div class="product_ratting">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>

                        </div>
                        <div class="single__product_sm mb-30">
                            <div class="produc_thumb">
                                <a href="product-details.html"><img src="site/img/product/pro_sm_2.png" alt=""></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                <div class="product_ratting">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                </div>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>

                        </div>
                        <div class="single__product_sm">
                            <div class="produc_thumb">
                                <a href="product-details.html"><img src="site/img/product/pro_sm_3.png" alt=""></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="#">Lotafaj una khdii</a></h3>
                                <div class="product_ratting">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="best_selling_single">
                        <div class="single__product_sm mb-30">
                            <div class="produc_thumb">
                                <a href="product-details.html"><img src="site/img/product/pro_sm_1.png" alt=""></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3> 
                                <div class="product_ratting">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>

                        </div>
                        <div class="single__product_sm mb-30">
                            <div class="produc_thumb">
                                <a href="product-details.html"><img src="site/img/product/pro_sm_2.png" alt=""></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                <div class="product_ratting">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                </div>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>

                        </div>
                        <div class="single__product_sm">
                            <div class="produc_thumb">
                                <a href="product-details.html"><img src="site/img/product/pro_sm_3.png" alt=""></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="#">Lotafaj una khdii</a></h3>
                                <div class="product_ratting">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Banner product section end-->


<!--Full Width  banner start-->
<div class="full_width_banner pb-110">
    <div class="single_banner">
        <a href="#"><img src="site/img/banner/8.jpg" alt="Shop Banner"></a>
    </div>
</div>
<!--Full Width Banner end-->

<!--Latest Post start-->
<div class="latest_post pb-70">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                    <h2>Latest News</h2>
                </div>
            </div>
        </div>
        <div class="row mt-60">
            <div class="col-lg-4 col-md-6">
                <div class="single_blog_post mb-40">
                    <div class="post_thumbnail">
                        <a href="blog-details.html"><img src="site/img/blog/1.jpg" alt=""></a>
                    </div>
                    <div class="post_content_meta">
                        <div class="post_meta">
                            <ul>            
                                <li>Posted March 20.</li>
                                <li>400+ View </li>
                                <li><a href="#"> 20+ Like</a></li>
                            </ul>
                        </div>
                        <div class="blog_post_desc">
                            <h2><a href="blog-details.html">Froome racing to spoil Yates’s pink Giro dream</a></h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry... </p>
                        </div>
                        <div class="read_more_btn">
                            <a href="blog-details.html">Read More <span><i class="zmdi zmdi-arrow-right"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_blog_post mb-40">
                    <div class="post_thumbnail">
                        <a href="blog-details.html"><img src="site/img/blog/2.jpg" alt=""></a>
                    </div>
                    <div class="post_content_meta">
                        <div class="post_meta">
                            <ul>            
                                <li>Posted March 20.</li>
                                <li>400+ View </li>
                                <li><a href="#"> 20+ Like</a></li>
                            </ul>
                        </div>
                        <div class="blog_post_desc">
                            <h2><a href="blog-details.html">Sed ut perspiciatis unde omnis iste natus sit</a></h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry... </p>
                        </div>
                        <div class="read_more_btn">
                            <a href="blog-details.html">Read More <span><i class="zmdi zmdi-arrow-right"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_blog_post mb-40">
                    <div class="post_thumbnail">
                        <a href="blog-details.html"><img src="site/img/blog/3.jpg" alt=""></a>
                    </div>
                    <div class="post_content_meta">
                        <div class="post_meta">
                            <ul>            
                                <li>Posted March 20.</li>
                                <li>400+ View </li>
                                <li><a href="#"> 20+ Like</a></li>
                            </ul>
                        </div>
                        <div class="blog_post_desc">
                            <h2><a href="blog-details.html">Quis autem vel eum tempore voluptate</a></h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry... </p>
                        </div>
                        <div class="read_more_btn">
                            <a href="blog-details.html">Read More <span><i class="zmdi zmdi-arrow-right"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div>
<!--Latest Post end-->


    
@endsection