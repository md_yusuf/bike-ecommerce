@extends('site.layouts.app')
@section('content')


<!-- Add your site or application content here -->
<!--Breadcrumb section-->
<div class="breadcrumb_section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_inner">
                    <ul>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><i class="zmdi zmdi-chevron-right"></i></li>
                        <li>product Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumb section end-->
<!--product Details Inner-->
<div class="product_details_inner left_sidebar ptb-110">
    <div class="container">
        <div class="row">
            <!--Product Tab Style start-->
            <div class="col-md-12 col-lg-5 col-12">
                <div class="product-details-img-content">
                    <div class="product-details-tab">
                        <div id="img-1" class="zoomWrapper single-zoom">
                            <a href="#">
                                <img id="zoom1" src="{{asset("storage/$product->featured_image") }}"
                                data-zoom-image="assets/img/product/pro_details/z-1.jpg" alt="big-1">
                            </a>
                        </div>
                        <div class="single-zoom-thumb mt-20">
                            @foreach ($product->images as $item)
                            <ul class="s-tab-zoom owl-carousel single-product-active">
                                <li>
                                    <a href="#" class="elevatezoom-gallery active" data-update=""
                                        data-image="assets/img/product/pro_details/big-2.jpg"
                                        data-zoom-image="assets/img/product/pro_details/z-1.jpg">
                                        <img src="{{ asset("storage/$item->path") }}" alt="zo-th-1"/>
                                    </a>
                                </li>
                            </ul>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
            <!--Product Tab Style End-->
            <form action="{{ url("/add_product/$product->id") }}" method="POST">
                @csrf

                <div class="col-md-12 col-lg-7 col-12">
                    <div class="product-details-content">
                        <h3>{{$product->name}}</h3>
                        <div class="rating-number">
                            <div class="product_rating">
                                <a href="#"><i class="zmdi zmdi-star"></i></a>
                                <a href="#"><i class="zmdi zmdi-star"></i></a>
                                <a href="#"><i class="zmdi zmdi-star"></i></a>
                                <a href="#"><i class="zmdi zmdi-star"></i></a>
                                <a href="#"><i class="zmdi zmdi-star-half"></i></a>
                            </div>
                            <div class="review_count">
                                <span>2 Ratting (S)</span>
                            </div>
                        </div>
                        <div class="price_amount">
                            <span class="current_price">${{$product->PriceAfterDiscount}}</span>
                            <span class="discount_price">{{$product->discount_amount}}</span>
                            <span class="old_price">${{$product->price}}</span>
                        </div>
                        <p> {{ $product->description }}</p>
                        <div class="product_variant_select">
                            <div class=" select-option-part">
                                <label>Size*</label>
                                @php
                                $size=$product->size;
                                $product_size=explode(',',$size);
                                @endphp
                                
                                {{-- <div class="colr">
                                    <label class="radio"><input type="radio" name="size"
                                            value="{{ $selected_size }}"><i></i>{{ $selected_size }}</label>
                                </div> --}}                              
                                <select name="size" class="nice-select">
                                    <option value="">- Please Select -</option>
                                    @foreach ($product_size as $selected_size)
                                    <option value="{{ $selected_size }}">{{ $selected_size }}</option>
                                    @endforeach
                                </select>
                                
                            </div>
                            <div class=" select-option-part">
                                <label>Color*</label>
                                @php
                                    $colour=$product->colour;
						            $product_colour=explode(',',$colour);
                                @endphp
                                <select name="name" class="nice-select">
                                    <option value="">- Please Select -</option> 
                                    @foreach ($product_colour as $selected_colour)	                                   
							        <option value="{{ $selected_colour }}">{{ $selected_colour }}</option>
							        @endforeach 
                                                                      
                                </select>
                            </div>
                        </div>
                        <div class="single_product_action  align-items-center">
                            <h5>Quantity:</h5>
                            <select id="country1" name="quantity" onchange="change_country(this.value)" class="frm-field required sect">
								{{ $now=1 }}
								{{ $last=$product->stock }}
									@for ($quantity = $now; $quantity <= $last; $quantity++)
        								<option value="{{ $quantity }}">{{ $quantity }}</option>
   									 @endfor             											
							</select>
                            {{-- <div class="cart-plus-minus">
                                <input type="text" value="02" name="qtybutton" class="cart-plus-minus-box">
                            </div> --}}
                            
                                <button type="submit" class="btn btn-outline-danger">Add to Cart</button>
                           
                            
                            {{-- <div class=" add_to_cart_btn text-center">
                                <a href="#">add to cart</a>
                            </div> --}}
                            {{-- <div class="wishlist">
                                <a href="#"><i class="zmdi zmdi-favorite-outline"></i></a>
                            </div> --}}
                        </div>
                        <div class="product_details_cat_list mt-35">
                            <div class="categories_label">
                                <span>Categories:</span>
                            </div>
                            <ul>
                                <li><a href="#">fashion</a></li>
                                <li><a href="#">electronics</a></li>
                                <li><a href="#">toys</a></li>
                                <li><a href="#">food</a></li>
                                <li><a href="#">jewellery</a></li>
                            </ul>
                        </div>
                        <div class="product_details_tag_list mtb-10">
                            <div class="tag_label">
                                <span>Tags : </span>
                            </div>
                            <ul>
                                <li><a href="#">fashion</a></li>
                                <li><a href="#">electronics</a></li>
                                <li><a href="#">toys</a></li>
                                <li><a href="#">food</a></li>
                                <li><a href="#">jewellery</a></li>
                            </ul>
                        </div>
                        <div class="product-share">
                            <div class="share_label">
                                <span>Share :</span>
                            </div>
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="zmdi zmdi-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="zmdi zmdi-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="zmdi zmdi-google-plus"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="zmdi zmdi-pinterest"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- Product Thumbnail Description Start -->
        <div class="product_desc_tab_container mt-100 ">

            <div class="thumb-desc-inner">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="product_desc_tab nav" role="tablist">
                            <li><a class="active" data-toggle="tab" href="#dtail">Description</a></li>
                            <li><a data-toggle="tab" href="#review">Reviews 1</a></li>
                        </ul>
                        <!-- Product Thumbnail Tab Content Start -->
                        <div class="tab-content thumb-content mt-30">
                            <div id="dtail" class="tab-pane fade show active">
                                <p> {{ $product->description }}</p>
                            </div>
                            
                            <div id="review" class="tab-pane fade">
                                <!-- Reviews Start -->
                                <div class="review">
                                    <div class="group-title">
                                        <h2>customer review</h2>
                                    </div>
                                    <ul class="review-list">
                                        <!-- Single Review List Start -->

                                        <!-- Single Review List Start -->
                                        @foreach ($comment_list as $item) 
                                        <li>
                                            <label>Posted on {{$item->created_at}}</label>
                                        </li>
                                        <li>                                                               
                                            <h3 class="review-title mb-15">Comment By {{$item->name}} : <br>
                                            <span><h5>Comment :{{$item->comment}}</h5></span></h3>
                                        </li>
                                        @endforeach
                                        <!-- Single Review List End -->
                                    </ul>
                                </div>
                                <!-- Reviews End -->
                                <!-- Reviews Start -->
                                <div class="review mt-20">
                                    

                                    <!-- Reviews Field Start -->
                                    
                                    <div class="riview-field mt-40">
                                        <div class="review_comment_box_inner">
                                            <form action="{{ url('/cart') }}" method="POST">
                                                @csrf

                                                <input type="hidden" id="product_id" name="product_id" value="{{ $product->id }}">
                                                <div class="form-group">
                                                    <label class="req" for="sure-name">Name</label>
                                                    <input type="text" name="name" id="sure-name" required="required">
                                                </div>
                                                <div class="form-group">
                                                    <label class="req" for="subject">Email</label>
                                                    <input type="email" name="email" id="subject" required="required">
                                                </div>
                                                {{-- <input type="hidden" name="product_id"  value="{{ $product->id }}" id="sure-name" required="required"> --}}

                                                <div class="form-group">
                                                    <label class="req" for="comments">Comment</label>
                                                    <textarea rows="5" id="comments" name="comment" required="required"></textarea>
                                                </div>
                                                <button type="submit" class="customer-btn">Submit</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Reviews Field Start -->
                                </div>
                                <!-- Reviews End -->
                            </div>
                        </div>
                        <!-- Product Thumbnail Tab Content End -->
                    </div>
                </div>
                <!-- Row End -->
            </div>
        </div>
        <!--Realted Product section start-->
        <div class="related_product_section mt-100">

            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2>Related Product</h2>
                    </div>
                </div>
            </div>
            <div class="row related_product_guttters owl-carousel mt-60">
                <div class="col-lg-3">
                    <div class="single__product">
                        <div class="produc_thumb">
                            <a href="#"><img src="{{ asset(" assets/img/product/home2/4.png") }}" alt=""></a>
                        </div>
                        <div class="product_hover">
                            <div class="product_action">
                                <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single__product">
                        <div class="produc_thumb">
                            <a href="#"><img src="assets/img/product/home2/5.png" alt=""></a>
                        </div>
                        <div class="product_hover">
                            <div class="product_action">
                                <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single__product">
                        <div class="produc_thumb">
                            <a href="#"><img src="assets/img/product/home2/6.png" alt=""></a>
                        </div>
                        <div class="product_hover">
                            <div class="product_action">
                                <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single__product">
                        <div class="produc_thumb">
                            <a href="#"><img src="assets/img/product/home2/7.png" alt=""></a>
                        </div>
                        <div class="product_hover">
                            <div class="product_action">
                                <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single__product">
                        <div class="produc_thumb">
                            <a href="#"><img src="assets/img/product/home2/8.png" alt=""></a>
                        </div>
                        <div class="product_hover">
                            <div class="product_action">
                                <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single__product">
                        <div class="produc_thumb">
                            <a href="#"><img src="assets/img/product/home2/9.png" alt=""></a>
                        </div>
                        <div class="product_hover">
                            <div class="product_action">
                                <a href="#" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                <a href="#" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                <a href="#" title="Compare"><i class="zmdi zmdi-refresh-alt"></i></a>
                            </div>
                            <div class="product__desc">
                                <h3><a href="product-details.html">Soffer Pro x33</a></h3>
                                <div class="price_amount">
                                    <span class="current_price">$2999.99</span>
                                    <span class="discount_price">-08%</span>
                                    <span class="old_price">$3700.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--Realted Product section end-->
    </div>
</div>
<!--product Details End-->


<!--Newsletter section end -->

@endsection