@extends('admin.layouts.app')
@section('page title')
<div class="row mb-2">
  <div class="col-sm-6">
    <h1>Category</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Dashboard</a></li>
      <li class="breadcrumb-item active">Category</li>
    </ol>
  </div>
</div>
@endsection
@section('contents')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Category List</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button> --}}
      <a class="btn btn-success" href="{{ url('/admin/categories/create') }}">Add New Category</a>
    </div>
  </div>
  <div class="card-body">
    <form method="POST" action="{{ url("/admin/categories/$category->id") }}">
      @csrf
      @method('put')
      <div class="card-body">
        <div class="form-group">
          <label style="background-color: aqua" for="exampleInputEmail1">Main Category</label>
          <select name="main_category_id" class="form-control">
            @foreach ($main_category as $key=>$item)
            <option style="background-color: aquamarine" value="{{$key}}" {{ $category->main_category_id==strval($key) ? 'selected' : '' }}>{{$item}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label style="background-color: aqua" for="exampleInputEmail1">Category name</label>
          <input type="text" name="name" class="form-control" value="{{ $category->name }}" 
            placeholder="Enter Category name">
        </div>

      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>
  <!-- /.card-body -->
  <div class="card-footer">

  </div>
  <!-- /.card-footer-->
</div>
@endsection