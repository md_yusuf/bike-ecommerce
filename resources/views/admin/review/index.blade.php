@extends('admin.layouts.app')
@section('page_title')
<div class="row mb-2">
  <div class="col-sm-6">
    <h1>Reviews</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Dashboard</a></li>
      <li class="breadcrumb-item active">Reviews</li>
    </ol>
  </div>
</div>
@endsection
@section('contents')
<!-- Default box -->
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Reviews List</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button> --}}
      {{-- <a class="btn btn-success pull-right" href="{{ url('/admin/categories/create') }}">Add New Category</a> --}}
    </div>
  </div>
  <div class="card-body">
    <table class="table table-bordered  ">
      <tr>
        <td style="text-align: center">User Name</td>
        <td style="text-align: center">User Email</td>
        <td style="text-align: center">Product Id</td>
        <td style="text-align: center">Product Name</td>
        <td style="text-align: center">Comment</td>
        <td style="text-align: center">Status</td>
        <td style="text-align: center">Verified </td>
        
        
      </tr>
    @foreach ($review as $item)
    <tr>
      <td>{{$item->name}}</td>
      <td>{{$item->email}}</td>
      <td >{{$item->product->id}}</td>         
      <td>{{$item->product->name}}</td>
      <td>{{$item->comment}}</td>
      <td>{{$item->status}}</td>
      <td>
        {{-- <a href="">approved?</a> --}}
        <a href="#" id="approved" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-id="{{$item->id}}" data-whatever="@mdo">Approved</a>
        {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button> --}}

        
      </td>
    </tr>
    @endforeach
      
      
    </table>
  </div>
  <!-- /.card-body -->
  {{-- <div class="card-footer">
    Footer
  </div> --}}
  <!-- /.card-footer-->
</div>
{{-- <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  approved?
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div> --}}

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Status change:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ url('/admin/reviews/update') }}" method="POST">
          @csrf

          <input type="hidden" id="review_id" name="review_id" value="">

          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Status value :</label>
            <input type="text" id="status_value" name="status_value" value="" class="form-control">
          </div>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save Changed</button>
      </div>
    </div>
  </div>

</form>
</div>

@endsection