@extends('admin.layouts.app')
@section('page_title')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1>Product</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Product</li>
        </ol>
    </div>
</div>
@endsection
@section('contents')
<div class="card">

    <div class="card-header">
        <h3 class="card-title">Product List</h3>
        <div class="card-tools">
            {{-- <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button> --}}
            <a class="btn btn-success pull-right" href="{{ url('/admin/products/create') }}">Add New Product</a>

        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <td>Name</td>
                <td>Price</td>
                <td>Discount Amonunt</td>
                <td>Stock</td>
                <td>Catgory</td>
                <th>Sizes</th>
                <th>Colours</th>
                <td>Featured Image</td>
                <td>Images</td>
                <td>action</td>
            </tr>
            @foreach ($product_list as $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td>{{ $item->price }}</td>
                <td>{{ $item->discount_amount }}</td>
                <td>{{ $item->stock }}</td>
                <td>{{ $item->category ? $item->category->name : "" }}</td>
                @php
                $size=$item->size;
                $product_size=explode(',',$size);
                @endphp
                <td>
                  @foreach ($product_size as $one_size)
                      {{ $one_size }}<br>
                  @endforeach
                </td>
                @php
                $colour=$item->colour;
                $product_colour=explode(',',$colour);
                @endphp
                <td>
                  @foreach ($product_colour as $one_colour)
                      {{ $one_colour }}<br>
                  @endforeach
                </td>
                <td>
                    <img class="img-fluid" style="width=110px;height:120px;" src="{{ asset("storage/$item->featured_image") }}">
                    
                </td>
                <td>
                    @foreach ($item->images as $item)
                    <li class="image_li">
                        <img style="width=150px;height:150px;" src="{{ asset("storage/$item->path") }}"
                        class="img-responsive_imge inline-block" alt="Responsive image" />
                    </li>
                    @endforeach
                </td>
                <td>

                    <a href="{{ url("/admin/products/$item->id/edit") }}" class="btn btn-info">Edit</a>
                    <form action="{{ url("/admin/products/$item->id") }}" method="post" style="display:inline"
                        onSubmit="return confirm('Are you sure you want to delete?')">
                        @csrf
                        @method("delete")
                        <input type="submit" class="btn btn-info" value="Delete">
                    </form>

                </td>
            </tr>
            @endforeach
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
    </div>
    <!-- /.card-footer-->
</div>
@endsection