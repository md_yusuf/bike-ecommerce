<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public function category()
    {
        return $this->belongsTo(Category::class , "category_id", "id");
    }

    public function images()
    {
        return $this->hasMany(Image::class , "product_id", "id");
    }

    public function selected_size()
    {
        return $this->hasOne(selected_size::class);
    }

    public function selected_colour()
    {
        return $this->hasOne(selected_color::class);
    }

    public function selected_quantity()
    {
        return $this->hasOne(selected_quantity::class);
    }
    public function reviews()
    {
        return $this->hasMany(Review::class , "product_id", "id");
    }

    public function getPriceAfterDiscountAttribute()
    {
        return $this->price - $this->discount_amount;
    }
}
