<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class MainCategory extends Enum
{
    const Bike =   0;
    const Parts =   1;
    const Tools = 2;
    const Brand = 3;

    public static function getDescription($value): string
{
    if ($value === self::Parts) {
        return "Motor Parts";
    }

    return parent::getDescription($value);
}


}
