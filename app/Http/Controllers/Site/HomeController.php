<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()

    {

        $cartCollection = \Cart::getContent();
        $data["cartCollection"] = $cartCollection;
        $data["category_list"] = Category::take(5)->get();
        $bike_product= Product::where('category_id', '=', 1)->take(5)->get();
        $data["bike_product"]= $bike_product;
        $bike_product= Product::where('category_id', '=', 18)->take(5)->get();
        $data["bike_product2"]= $bike_product;
        $bike_product= Product::where('category_id', '=', 3)->take(5)->get();
        $data["bike_product3"]= $bike_product;
        $bike_product= Product::where('category_id', '=', 2)->take(5)->get();
        $data["bike_product4"]= $bike_product;
        $bike_product= Product::where('category_id', '=', 7)->take(5)->get();
        $data["bike_product5"]= $bike_product;
        $bestProduct= Product::where('discount_amount', '=', 0)
        ->take(6)
        ->orderBy('discount_amount', 'desc')
        ->get();
        $data["bestProduct"] = $bestProduct ;

        $hotDeals = Product::take(6)->where('discount_amount' ,'>=',10000)->get();
        $data["hotDeals"] = $hotDeals;
       return view('site.home',$data);
    }

    public function product($id)
    {
        $cartCollection = \Cart::getContent();
        $data["cartCollection"] = $cartCollection;
       
        $product= Product::find($id);
        $data["product"] = $product ;

        $data["comment_list"] = DB::table('reviews')
            ->where('status', '=', 1)
            ->where('product_id', '=',$id)
            ->get();
        //return $data;
        return view('site.product.single', $data);
    }
    
    
}
