<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\selected_color;
use App\Models\selected_quantity;
use App\Models\selected_size;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function add_product($product_id, Request $request)
    {
        $colour =new selected_color();
        $colour->product_id=$request->id;
        $colour->name=$request->name;
        $colour->save();

        $size =new selected_size();
        $size->product_id=$request->id;
        $size->name=$request->size;
        $size->save();

        $quantity =new selected_quantity();
        $quantity->product_id=$request->id;
        $quantity->amount=$request->quantity;
        $quantity->save();

       $product = Product::find($product_id);
       $newAmount=0;
        if($product->selected_quantity->amount!=null)
        {
            $newAmount=$product->selected_quantity->amount;
        }else{
            $newAmount=1;
        }
        if ($product) {
            \Cart::add(array(
                'id' => $product->id, // inique row ID
                'name' => $product->name,
                'price' => $product->price-$product->discount_amount,
                'quantity' =>$newAmount,
                'attributes' => array(
                    'featured_image' => $product->featured_image,
                    'size' => $product->selected_size->name,
                    'colour' => $product->selected_colour->name,
                  )
            ));
            return redirect('/cart')->with('msg','Cart add successfully');
        } else {
            return redirect()->back()->with('msg', 'Cart not added');  
        }
        
    }
    public function store(Request $request)
    {
        $colour =new selected_color();
        $colour->name=$request->name;

        $size = new selected_size();
        $size->name = $request->name ;
    }

    public function cart()
    {
        $cartCollection = \Cart::getContent();
        // dd($cartCollection);
        $data["cartCollection"] = $cartCollection;
        return view('Site.cart.cart',$data);
    }
    public function remove_product($id)
    {
        \Cart::remove($id);
        return redirect()->back();
    }
    // public function remove_product_home($id)
    // {
    //     \Cart::remove($id);
    //     return redirect("/");
    // }

    public function cart_remove_one_product($product_id)
    {

        $product = \Cart::get($product_id);
        if ($product->quantity == 1) {
            \Cart::remove($product_id);
        } else {
            \Cart::update($product_id, array(
                'quantity' => -1,
            ));
        }
        return redirect()->back();
    }

    public function cart_add_one_product($product_id)
    {
        $product = \Cart::get($product_id);
        // return $product->quantity;
        // $product = $this->productRepo->myFind($product_id);
        \Cart::update($product_id, array(
            'quantity' => 1,
        ));

        //$a= $product->price * $product->quantity;
        return redirect()->back();
    }

    public function goCart()
    {
        $cartCollection = \Cart::getContent();
        // dd($cartCollection);
        $data["cartCollection"] = $cartCollection;
        return view('Site.layouts.topmenu',$data);
    }

    public function checkout()
    {
        $cartCollection = \Cart::getContent();
        // dd($cartCollection);
        $data["cartCollection"] = $cartCollection;
        return view('Site.cart.checkout',$data);
    }
    
}
